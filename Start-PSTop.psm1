<#

.SYNOPSIS

Shows computer stats in real time

.DESCRIPTION

Shows CPU, RAM, Processes stats in real time

.PARAMETER Top 

Number of processes to show


.PARAMETER Refresh

Time in secods to refresh screen

.PARAMETER OrderBy

Get-Process property for ordering. Defaut is CPU.


.EXAMPLE

Start-PSTop -Top 10 -Refresh 3 -OrderBy CPU


.NOTES

Tested with PS v5 only.

#>
Function Start-PSTop
{
    [CmdletBinding()]
    param(  [int]$Top = 20,
            [int]$Refresh = 3,
            [ValidateSet("Handles", "CPU", "ProcessName", "PM", "VM", "NPM", "ID")]
            [string]$OrderBy = "CPU"
    )

    Clear-Host
    while ($true) {
        $proc = [math]::Round(((Get-Counter '\Processor(_Total)\% Processor Time').CounterSamples.CookedValue),1)
        $os = Get-CimInstance -classname Win32_OperatingSystem -ErrorAction stop
        $totalRAM = [int]($os.TotalVisibleMemorySize/1mb)
        $freeRAM = [math]::Round($os.FreePhysicalMemory/1mb,2)
        $usedram = [int]($os.TotalVisibleMemorySize - $os.FreePhysicalMemory)
        $freeRAMProc = [math]::Round(($usedram/$os.TotalVisibleMemorySize)*100,2)
        $uptime = [datetime]::Now - $os.LastBootUpTime
        Write-Output "Computername: $env:COMPUTERNAME"
        Write-Output "Uptime: Days $($uptime.Days) Hours $($uptime.Hours) Minutes $($uptime.Minutes)"
        Write-Output "CPU: Used ${proc}%"
        Write-Output "RAM: Total ${totalRAM}Gb, Free ${freeRAM}Gb, Used ${freeRAMProc}%"
        Get-Process | Sort -Descending $OrderBy | Select -First $Top | Format-Table -AutoSize
        Sleep -Seconds $Refresh
        [console]::setcursorposition(0,0)    
    }
    
}



