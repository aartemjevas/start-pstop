![m2sX0mmcyr.gif](https://bitbucket.org/repo/RLg87b/images/2457818591-m2sX0mmcyr.gif)
```
#!powershell

<#

.SYNOPSIS

Shows computer stats in real time

.DESCRIPTION

Shows CPU, RAM, Processes stats in real time

.PARAMETER Top 

Number of processes to show


.PARAMETER Refresh

Time in secods to refresh screen

.PARAMETER OrderBy

Get-Process property for ordering. Defaut is CPU.


.EXAMPLE

Start-PSTop -Top 10 -Refresh 3 -OrderBy CPU


.NOTES

Tested with PS v5 only.

#>
```